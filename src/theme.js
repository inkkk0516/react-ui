/* eslint-disable import/no-anonymous-default-export */
export default {
    primaryColor: "#4F9DDE",
    green: "#34D859",
    gray: "rgba(24, 28, 47, 0.2)"
}