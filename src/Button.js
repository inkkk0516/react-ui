import React from 'react'
import styled from 'styled-components'

//下面一定要写分号！！！
const StyleButton = styled.div`
    width: ${({width})=>width || "80px"};
    background-color: ${({theme})=>theme.primaryColor};
`

function Button({width, label,onClick,children}) {
    return(
        <StyleButton width={width} onClick={onClick}>
            <button>{label}</button>
            {children}
        </StyleButton>
    ) 
}

export default Button;