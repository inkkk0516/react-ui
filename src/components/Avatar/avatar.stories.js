/* eslint-disable import/no-anonymous-default-export */
import React from 'react'
import Avator from ".";

import face1 from 'assets/images/face-male-1.jpg'
import face2 from 'assets/images/face-male-2.jpg'
import face3 from 'assets/images/face-male-3.jpg'
import face4 from 'assets/images/face-male-4.jpg'

import "story.css";

export default {
    title: "Avator",
    component: Avator,
}

export const Default = () => {
    return <Avator src={face1}/>
}

export const Sizes = () => {
    return(
        <div className="row-elements">
            <Avator src={face1} size="48px" />
            <Avator src={face2} size="56px" />
            <Avator src={face3} size="64px" />
            <Avator src={face4} size="72px" />
        </div>
    ) 
}

export const WithStatus = () => {
    return(
        <div className="row-elements">
            <Avator src={face1} status="online" />
            <Avator src={face2} status="offline" />
            <Avator src={face4} status="offline" size="72px" statusIconSize="12px"/>
        </div>
    ) 
}