import React from 'react'
import PropTypes from 'prop-types'
import StyledAvatar, { AvatorClip, AvatorImage, StatusIcon } from './style'

/**
 * 
 * @param {src} 图片地址 
 * @param {size} 图片尺寸 
 * @param {status} 状态 
 * @param {statusIconSize} 图标尺寸 
 */
function Avator({src, size="48px", status, statusIconSize="8px", ...rest}) {
    return (
        <StyledAvatar {...rest}>
            {/* status是空值时不显示 */}
            {status && (
                <StatusIcon status={status} size={statusIconSize}></StatusIcon>
            )}
            <AvatorClip size={size}>
                <AvatorImage src={src} alt=""/>
            </AvatorClip>
        </StyledAvatar>
    )
}
Avator.propTypes = {
    src: PropTypes.string.isRequired,
    size: PropTypes.string,
    status: PropTypes.oneOf(["online","offline"]),
    statusIconSize: PropTypes.string,
}

export default Avator

